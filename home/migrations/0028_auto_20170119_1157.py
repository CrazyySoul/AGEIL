# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-01-19 10:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0027_auto_20170119_1152'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cursuspossible',
            old_name='codePostale',
            new_name='codePostal',
        ),
    ]
