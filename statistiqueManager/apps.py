from django.apps import AppConfig


class StatistiquemanagerConfig(AppConfig):
    name = 'statistiqueManager'
